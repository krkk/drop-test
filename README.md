a simple program designed to check if programs support copying or dragging files to sandboxed applications.

building, installing and running:

```
flatpak-builder --user --install --force-clean build com.example.droptest.yml
flatpak run com.example.droptest
```
