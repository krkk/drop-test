#include <KF5/KCoreAddons/KUrlMimeData>
#include <QAction>
#include <QApplication>
#include <QClipboard>
#include <QDebug>
#include <QMainWindow>
#include <QMimeData>
#include <QDropEvent>
#include <QFile>

static void testFileAccessFromMimeData(QMimeData const* mimeData)
{
    // shouldn't QMimeData::urls() just implicitly call the Desktop portal?
    auto list = KUrlMimeData::urlsFromMimeData(mimeData, KUrlMimeData::PreferLocalUrls);
    auto path = list.first().toLocalFile();
    qDebug().noquote() << QString("formats={%1}, path[0]=%2, accessible=%3")
        .arg(mimeData->formats().join(", "), path, QFile::exists(path) ? "YES!" : "no");
}

class MainWindow : public QMainWindow {
public:
    MainWindow() {
        setWindowTitle("Drop or paste file");
        setAcceptDrops(true);

        QAction* action = new QAction("Paste file", this);
        action->setShortcuts(QKeySequence::Paste);
        connect(action, &QAction::triggered, [] {
            QMimeData const* mimeData = QGuiApplication::clipboard()->mimeData();
            testFileAccessFromMimeData(mimeData);
        });
        addAction(action);
    }

protected:
    virtual void dragEnterEvent(QDragEnterEvent *event) override {
        if (event->mimeData()->hasUrls())
            event->acceptProposedAction();
    }

    virtual void dropEvent(QDropEvent *event) override {
        event->acceptProposedAction();
        testFileAccessFromMimeData(event->mimeData());
    }
};

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    MainWindow w;
    w.show();
    return app.exec();
}
